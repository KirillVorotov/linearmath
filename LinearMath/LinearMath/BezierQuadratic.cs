﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LinearMath
{
    /// <summary>
    /// Quadratic Bezier Curve
    /// </summary>
    [Serializable]
    public struct BezierQuadratic : IEquatable<BezierQuadratic>, IBezierCurve
    {
        #region Fields

        /// <summary>
        /// First point of the BezierQuadratic curve.
        /// </summary>
        public Vector3f P0;

        /// <summary>
        /// Second point of the BezierQuadratic curve.
        /// </summary>
        public Vector3f P1;

        /// <summary>
        /// Third point of the BezierQuadratic curve.
        /// </summary>
        public Vector3f P2;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs a new quadratic Bezier curve.
        /// </summary>
        /// <param name="p0">First point of the BezierQuadratic curve.</param>
        /// <param name="p1">Second point of the BezierQuadratic curve.</param>
        /// <param name="p2">Third point of the BezierQuadratic curve.</param>
        /// <param name="p3">Fourth point of the BezierQuadratic curve.</param>
        public BezierQuadratic(Vector3f p0, Vector3f p1, Vector3f p2)
        {
            P0 = p0;
            P1 = p1;
            P2 = p2;
        }

        /// <summary>
        /// Constructs a new linear quadratic Bezier curve from <param name="start"></param> point to <param name="end"></param> point.
        /// </summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        public BezierQuadratic(Vector3f start, Vector3f end)
        {
            P0 = start;
            P2 = end;
            P1 = (P0 + P2) * 0.5f;
        }

        #endregion

        #region Public Members

        public Vector3f this[int index]
        {
            get
            {
                if (index == 0) return P0;
                else if (index == 1) return P1;
                else if (index == 2) return P2;
                throw new IndexOutOfRangeException("You tried to access this Bezier curve at index: " + index);
            }
            set
            {
                if (index == 0) P0 = value;
                else if (index == 1) P1 = value;
                else if (index == 2) P2 = value;
                throw new IndexOutOfRangeException("You tried to access this Bezier curve at index: " + index);
            }
        }

        public Vector3f StartPoint
        {
            get { return P0; }
            set { P0 = value; }
        }

        public Vector3f EndPoint
        {
            get { return P2; }
            set { P2 = value; }
        }

        public Vector3f StartControl
        {
            get { return P1; }
            set { P1 = value; }
        }

        public Vector3f EndControl
        {
            get { return P1; }
            set { P1 = value; }
        }

        #region Instance

        /// <summary>
        /// Returns a point on the Bezier curve depending on t.
        /// </summary>
        /// <param name="t">Must be greater than or equal to zero and less than or equal to one.</param>
        public Vector3f GetPoint(float t)
        {
            return (1f - t) * (1f - t) * P0 + 2f * t * (1f - t) * P1 + t * t * P2;
        }

        /// <summary>
        /// Returns the length of the curve
        /// </summary>
        public float Length()
        {
            return Length(this);
        }

        /// <summary>
        /// Swaps the points of the curve
        /// </summary>
        public BezierQuadratic Swap()
        {
            return Swap(this);
        }

        /// <summary>
        /// Swaps the points of the curve
        /// </summary>
        public IBezierCurve SwapCurve()
        {
            return Swap(this);
        }

        /// <summary>
        /// Returns a copy of the quadratic Bezier curve with points swapped.
        /// </summary>
        public BezierQuadratic Swapped()
        {
            BezierQuadratic curve = this;
            curve.Swap();
            return curve;
        }

        /// <summary>
        /// Splits the curve at given position t.
        /// (De Casteljau's algorithm)
        /// </summary>
        /// <param name="t">Must be greater than or equal to zero and less than or equal to one.</param>
        public BezierQuadratic[] SplitAt(float t)
        {
            return SplitAt(this, t);
        }

        /// <summary>
        /// Splits the curve at given position t.
        /// (De Casteljau's algorithm)
        /// </summary>
        /// <param name="t">Must be greater than or equal to zero and less than or equal to one.</param>
        public IBezierCurve[] SplitCurveAt(float t)
        {
            var split = SplitAt(this, t);
            return new IBezierCurve[] { split[0], split[1] };
        }

        #endregion

        #region Static

        public static float Length(BezierQuadratic curve)
        {
            var A0 = curve.P1 - curve.P0;
            var A1 = curve.P0 - 2f * curve.P1 + curve.P2;
            
            if(A1 != Vector3f.Zero)
            {
                var c = 4f * Vector3f.Dot(A1, A1);
                var b = 8f * Vector3f.Dot(A0, A1);
                var a = 4f * Vector3f.Dot(A0, A0);
                var q = 4f * a * c - b * b;
                var twoCpB = 2f * c + b;
                var sumCBA = c + b + a;
                var mult0 = 0.25f / c;
                var mult1 = q / (8f * Math.Pow(c, 1.5f));
                var result = mult0 * (twoCpB * Math.Sqrt(sumCBA) - b * Math.Sqrt(a)) + mult1 * (Math.Log(2 * Math.Sqrt(c * sumCBA) + twoCpB) - Math.Log(2 * Math.Sqrt(c * a) + b));
                return (float)result;
            }
            else
            {
                return 2f * A0.Length;
            }
        }

        /// <summary>
        /// Swaps the points of the given curve.
        /// </summary>
        /// <param name="curve"></param>
        public static BezierQuadratic Swap(BezierQuadratic curve)
        {
            var p0 = curve.P0;
            var p2 = curve.P2;
            curve.P0 = p2;
            curve.P2 = p0;
            return curve;
        }

        /// <summary>
        /// Splits the curve at given position t.
        /// (De Casteljau's algorithm)
        /// </summary>
        /// <param name="curve"></param>
        /// <param name="t">Must be greater than or equal to zero and less than or equal to one.</param>
        public static BezierQuadratic[] SplitAt(BezierQuadratic curve, float t)
        {
            var p01 = curve.P0 * (1f - t) + curve.P1 * t;
            var p12 = curve.P1 * (1f - t) + curve.P2 * t;

            var pMiddle = curve.GetPoint(t);

            return new[]
            {
                new BezierQuadratic(curve.P0, p01, pMiddle),
                new BezierQuadratic(pMiddle, p12, curve.P2)
            };
        }

        #region Fields



        #endregion

        #endregion

        #region Operators

        /// <summary>
        /// Translates an instance in a given direction.
        /// </summary>
        /// <param name="curve"></param>
        /// <param name="vector"></param>
        public static BezierQuadratic operator +(BezierQuadratic curve, Vector3f vector)
        {
            curve.P0 += vector;
            curve.P1 += vector;
            curve.P2 += vector;
            return curve;
        }

        /// <summary>
        /// Translates an instance in a direction opposite to the given.
        /// </summary>
        /// <param name="curve"></param>
        /// <param name="vector"></param>
        public static BezierQuadratic operator -(BezierQuadratic curve, Vector3f vector)
        {
            curve.P0 -= vector;
            curve.P1 -= vector;
            curve.P2 -= vector;
            return curve;
        }

        /// <summary>
        /// Multiplies an instance by a scalar.
        /// </summary>
        /// <param name="curve">The instance.</param>
        /// <param name="scale">The scalar.</param>
        /// <returns>The result of the calculation.</returns>
        public static BezierQuadratic operator *(BezierQuadratic curve, float scale)
        {
            curve.P0 *= scale;
            curve.P1 *= scale;
            curve.P2 *= scale;
            return curve;
        }

        /// <summary>
        /// Multiplies an instance by a scalar.
        /// </summary>
        /// <param name="scale">The scalar.</param>
        /// /// <param name="curve">The instance.</param>
        /// <returns>The result of the calculation.</returns>
        public static BezierQuadratic operator *(float scale, BezierQuadratic curve)
        {
            curve.P0 *= scale;
            curve.P1 *= scale;
            curve.P2 *= scale;
            return curve;
        }

        /// <summary>
        /// Divides an instance by a scalar.
        /// </summary>
        /// <param name="curve">The instance.</param>
        /// <param name="scale">The scalar.</param>
        /// <returns>The result of the calculation.</returns>
        public static BezierQuadratic operator /(BezierQuadratic curve, float scale)
        {
            float mult = 1.0f / scale;
            curve.P0 *= mult;
            curve.P1 *= mult;
            curve.P2 *= mult;
            return curve;
        }

        /// <summary>
        /// Compares two instances for equality.
        /// </summary>
        /// <param name="left">The first instance.</param>
        /// <param name="right">The second instance.</param>
        /// <returns>True, if left equals right; false otherwise.</returns>
        public static bool operator ==(BezierQuadratic left, BezierQuadratic right)
        {
            return left.Equals(right);
        }

        /// <summary>
        /// Compares two instances for inequality.
        /// </summary>
        /// <param name="left">The first instance.</param>
        /// <param name="right">The second instance.</param>
        /// <returns>True, if left does not equa lright; false otherwise.</returns>
        public static bool operator !=(BezierQuadratic left, BezierQuadratic right)
        {
            return !left.Equals(right);
        }

        #endregion

        #region Overrides

        private static string listSeparator = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ListSeparator;

        /// <summary>
        /// Returns a System.String that represents the current BezierQuadratic.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return String.Format("{0}{3} {1}{3} {2}", P0.ToString(), P1.ToString(), P2.ToString(), listSeparator);
        }

        /// <summary>
        /// Returns the hashcode for this instance.
        /// </summary>
        /// <returns>A System.Int32 containing the unique hashcode for this instance.</returns>
        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = this.P0.GetHashCode();
                hashCode = (hashCode * 397) ^ this.P1.GetHashCode();
                hashCode = (hashCode * 397) ^ this.P2.GetHashCode();
                return hashCode;
            }
        }

        /// <summary>
        /// Indicates whether this instance and a specified object are equal.
        /// </summary>
        /// <param name="obj">The object to compare to.</param>
        /// <returns>True if the instances are equal; false otherwise.</returns>
        public override bool Equals(object obj)
        {
            if (!(obj is BezierQuadratic))
                return false;

            return this.Equals((BezierQuadratic)obj);
        }

        #endregion

        #endregion

        #region IEquitable<BezierQuadratic> Members

        /// <summary>Indicates whether the current Bezier curve is equal to another Bezier curve.</summary>
        /// <param name="other">A curve to compare with this curve.</param>
        /// <returns>true if the current curve is equal to the curve parameter; otherwise, false.</returns>
        public bool Equals(BezierQuadratic other)
        {
            return
                P0 == other.P0 &&
                P1 == other.P1 &&
                P2 == other.P2;
        }

        #endregion
    }
}
