﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LinearMath
{
    [Serializable]
    public struct BezierLinear : IEquatable<BezierLinear>, IBezierCurve
    {
        #region Fields

        /// <summary>
        /// First point of the BezierCubic curve.
        /// </summary>
        public Vector3f P0;

        /// <summary>
        /// Second point of the BezierCubic curve.
        /// </summary>
        public Vector3f P1;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs a new linear Bezier curve from <param name="start"></param> point to <param name="end"></param> point.
        /// </summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        public BezierLinear(Vector3f start, Vector3f end)
        {
            P0 = start;
            P1 = end;
        }

        #endregion

        #region Public members

        public Vector3f this[int index]
        {
            get
            {
                if (index == 0) return P0;
                else if (index == 1) return P1;
                throw new IndexOutOfRangeException("You tried to access this Bezier curve at index: " + index);
            }
            set
            {
                if (index == 0) P0 = value;
                else if (index == 1) P1 = value;
                throw new IndexOutOfRangeException("You tried to access this Bezier curve at index: " + index);
            }
        }

        public Vector3f StartPoint
        {
            get { return P0; }
            set { P0 = value; }
        }

        public Vector3f EndPoint
        {
            get { return P1; }
            set { P1 = value; }
        }

        public Vector3f StartControl
        {
            get { return P0; }
            set { P0 = value; }
        }

        public Vector3f EndControl
        {
            get { return P1; }
            set { P1 = value; }
        }

        #region Instance

        /// <summary>
        /// Returns a point on the Bezier curve depending on t.
        /// </summary>
        /// <param name="t">Must be greater than or equal to zero and less than or equal to one.</param>
        public Vector3f GetPoint(float t)
        {
            return P0 + t * (P1 - P0);
        }

        /// <summary>
        /// Returns the length of the curve
        /// </summary>
        public float Length()
        {
            return Length(this);
        }

        /// <summary>
        /// Swaps the points of the curve
        /// </summary>
        public BezierLinear Swap()
        {
            return Swap(this);
        }

        /// <summary>
        /// Swaps the points of the curve
        /// </summary>
        public IBezierCurve SwapCurve()
        {
            return Swap(this);
        }

        /// <summary>
        /// Returns a copy of the quadratic Bezier curve with points swapped.
        /// </summary>
        public BezierLinear Swapped()
        {
            BezierLinear curve = this;
            curve.Swap();
            return curve;
        }

        /// <summary>
        /// Splits the curve at given position t.
        /// (De Casteljau's algorithm)
        /// </summary>
        /// <param name="t">Must be greater than or equal to zero and less than or equal to one.</param>
        public BezierLinear[] SplitAt(float t)
        {
            return SplitAt(this, t);
        }

        /// <summary>
        /// Splits the curve at given position t.
        /// (De Casteljau's algorithm)
        /// </summary>
        /// <param name="t">Must be greater than or equal to zero and less than or equal to one.</param>
        public IBezierCurve[] SplitCurveAt(float t)
        {
            var split = SplitAt(this, t);
            return new IBezierCurve[] { split[0], split[1] };
        }

        #endregion

        #region Static

        public static float Length(BezierLinear curve)
        {
            return (curve.P1 - curve.P0).Length;
        }

        /// <summary>
        /// Swaps the points of the given curve.
        /// </summary>
        /// <param name="curve"></param>
        public static BezierLinear Swap(BezierLinear curve)
        {
            var p0 = curve.P0;
            curve.P0 = curve.P1;
            curve.P1 = p0;
            return curve;
        }

        /// <summary>
        /// Splits the curve at given position t.
        /// (De Casteljau's algorithm)
        /// </summary>
        /// <param name="curve"></param>
        /// <param name="t">Must be greater than or equal to zero and less than or equal to one.</param>
        public static BezierLinear[] SplitAt(BezierLinear curve, float t)
        {
            var pMiddle = curve.GetPoint(t);
            return new[]
            {
                new BezierLinear(curve.P0, pMiddle),
                new BezierLinear(pMiddle, curve.P1)
            };
        }

        #endregion

        #region Operators



        #endregion

        #region Overrides

        private static string listSeparator = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ListSeparator;

        /// <summary>
        /// Returns a System.String that represents the current BezierCubic.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return String.Format("{0}{2} {1}", P0.ToString(), P1.ToString(), listSeparator);
        }

        /// <summary>
        /// Returns the hashcode for this instance.
        /// </summary>
        /// <returns>A System.Int32 containing the unique hashcode for this instance.</returns>
        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = this.P0.GetHashCode();
                hashCode = (hashCode * 397) ^ this.P1.GetHashCode();
                return hashCode;
            }
        }

        /// <summary>
        /// Indicates whether this instance and a specified object are equal.
        /// </summary>
        /// <param name="obj">The object to compare to.</param>
        /// <returns>True if the instances are equal; false otherwise.</returns>
        public override bool Equals(object obj)
        {
            if (!(obj is BezierLinear))
                return false;

            return this.Equals((BezierLinear)obj);
        }

        #endregion

        #endregion

        #region IEquitable<BezierLinear> Members

        /// <summary>Indicates whether the current Bezier curve is equal to another Bezier curve.</summary>
        /// <param name="other">A curve to compare with this curve.</param>
        /// <returns>true if the current curve is equal to the curve parameter; otherwise, false.</returns>
        public bool Equals(BezierLinear other)
        {
            return
                P0 == other.P0 &&
                P1 == other.P1;
        }

        #endregion
    }
}
