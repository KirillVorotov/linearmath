﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LinearMath
{
    public interface IBezierCurve
    {
        Vector3f StartPoint { get; set; }
        Vector3f EndPoint { get; set; }
        Vector3f StartControl { get; set; }
        Vector3f EndControl { get; set; }
        Vector3f GetPoint(float t);
        IBezierCurve[] SplitCurveAt(float t);
        IBezierCurve SwapCurve();
        float Length();
    }
}
