﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LinearMath
{
    /// <summary>
    /// Cubic Bezier Curve
    /// </summary>
    [Serializable]
    public struct BezierCubic : IEquatable<BezierCubic>, IBezierCurve
    {
        #region Fields

        /// <summary>
        /// First point of the BezierCubic curve.
        /// </summary>
        public Vector3f P0;

        /// <summary>
        /// Second point of the BezierCubic curve.
        /// </summary>
        public Vector3f P1;

        /// <summary>
        /// Third point of the BezierCubic curve.
        /// </summary>
        public Vector3f P2;

        /// <summary>
        /// Fourth point of the BezierCubic curve.
        /// </summary>
        public Vector3f P3;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs a new cubic Bezier curve.
        /// </summary>
        /// <param name="p0">First point of the BezierCubic curve.</param>
        /// <param name="p1">Second point of the BezierCubic curve.</param>
        /// <param name="p2">Third point of the BezierCubic curve.</param>
        /// <param name="p3">Fourth point of the BezierCubic curve.</param>
        public BezierCubic(Vector3f p0, Vector3f p1, Vector3f p2, Vector3f p3)
        {
            P0 = p0;
            P1 = p1;
            P2 = p2;
            P3 = p3;
        }

        /// <summary>
        /// Constructs a new linear cubic Bezier curve from <param name="start"></param> point to <param name="end"></param> point.
        /// </summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        public BezierCubic(Vector3f start, Vector3f end)
        {
            P0 = start;
            P3 = end;
            P1 = (P0 * 2f + P3) / 3f;
            P2 = (P0 + P3 * 2f) / 3f;
        }

        #endregion

        #region Public Members

        public Vector3f this[int index]
        {
            get
            {
                if (index == 0) return P0;
                else if (index == 1) return P1;
                else if (index == 2) return P2;
                else if (index == 3) return P3;
                throw new IndexOutOfRangeException("You tried to access this Bezier curve at index: " + index);
            }
            set
            {
                if (index == 0) P0 = value;
                else if (index == 1) P1 = value;
                else if (index == 2) P2 = value;
                else if (index == 3) P3 = value;
                throw new IndexOutOfRangeException("You tried to access this Bezier curve at index: " + index);
            }
        }

        public Vector3f StartPoint
        {
            get { return P0; }
            set { P0 = value; }
        }

        public Vector3f EndPoint
        {
            get { return P3; }
            set { P3 = value; }
        }

        public Vector3f StartControl
        {
            get { return P1; }
            set { P1 = value; }
        }

        public Vector3f EndControl
        {
            get { return P2; }
            set { P2 = value; }
        }

        #region Instance

        /// <summary>
        /// Returns a point on the Bezier curve depending on t.
        /// </summary>
        /// <param name="t">Must be greater than or equal to zero and less than or equal to one.</param>
        public Vector3f GetPoint(float t)
        {
            return (1f - t) * (1f - t) * (1f - t) * P0 + 3f * t * (1f - t) * (1f - t) * P1 + 3f * t * t * (1f - t) * P2 + t * t * t * P3;
        }

        /// <summary>
        /// Length of the curve obtained via line interpolation
        /// </summary>
        public float LengthInterpolated()
        {
            return LengthInterpolated(this);
        }

        /// <summary>
        /// Returns the length of the curve
        /// </summary>
        public float Length()
        {
            return LengthInterpolated(this);
        }

        /// <summary>
        /// Swaps the points of the curve
        /// </summary>
        public BezierCubic Swap()
        {
            return Swap(this);
        }

        /// <summary>
        /// Swaps the points of the curve
        /// </summary>
        public IBezierCurve SwapCurve()
        {
            return Swap(this);
        }

        /// <summary>
        /// Returns a copy of the cubic Bezier curve with points swapped.
        /// </summary>
        public BezierCubic Swapped()
        {
            BezierCubic curve = this;
            curve.Swap();
            return curve;
        }

        /// <summary>
        /// Splits the curve at given position t.
        /// (De Casteljau's algorithm)
        /// </summary>
        /// <param name="t">Must be greater than or equal to zero and less than or equal to one.</param>
        public BezierCubic[] SplitAt(float t)
        {
            return SplitAt(this, t);
        }

        /// <summary>
        /// Splits the curve at given position t.
        /// (De Casteljau's algorithm)
        /// </summary>
        /// <param name="t">Must be greater than or equal to zero and less than or equal to one.</param>
        public IBezierCurve[] SplitCurveAt(float t)
        {
            var split = SplitAt(this, t);
            return new IBezierCurve[] { split[0], split[1] };
        }

        #endregion

        #region Static

        /// <summary>
        /// Length of the curve obtained via line interpolation
        /// </summary>
        public static float LengthInterpolated(BezierCubic curve)
        {
            var dt = LineInterpolationPrecision / (curve.P3 - curve.P0).Length;
            var length = 0f;
            for (float t = dt; t < 1; t += dt)
            {
                length += (curve.GetPoint(t - dt) - curve.GetPoint(t)).Length;
            }
            return length;
        }

        /// <summary>
        /// Swaps the points of the given curve.
        /// </summary>
        /// <param name="curve"></param>
        public static BezierCubic Swap(BezierCubic curve)
        {
            var p0 = curve.P0;
            var p1 = curve.P1;
            var p2 = curve.P2;
            var p3 = curve.P3;
            curve.P0 = p3;
            curve.P1 = p2;
            curve.P2 = p1;
            curve.P3 = p0;
            return curve;
        }

        /// <summary>
        /// Splits the curve at given position t.
        /// (De Casteljau's algorithm)
        /// </summary>
        /// <param name="curve"></param>
        /// <param name="t">Must be greater than or equal to zero and less than or equal to one.</param>
        public static BezierCubic[] SplitAt(BezierCubic curve, float t)
        {
            var p01 = curve.P0 * (1f - t) + curve.P1 * t;
            var p12 = curve.P1 * (1f - t) + curve.P2 * t;
            var p23 = curve.P2 * (1f - t) + curve.P3 * t;

            var p012 = p01 * (1f - t) + p12 * t;
            var p123 = p12 * (1f - t) + p12 * t;

            var pMiddle = curve.GetPoint(t);

            return new[]
            {
                new BezierCubic(curve.P0, p01, p012, pMiddle), new BezierCubic(pMiddle, p123, p23, curve.P3)
            };
        }

        #region Fields

        public static float LineInterpolationPrecision = 0.05f;
        public static float InterpolationPrecision = 0.001f;

        #endregion

        #endregion

        #region Operators

        /// <summary>
        /// Translates an instance in a given direction.
        /// </summary>
        /// <param name="curve"></param>
        /// <param name="vector"></param>
        public static BezierCubic operator +(BezierCubic curve, Vector3f vector)
        {
            curve.P0 += vector;
            curve.P1 += vector;
            curve.P2 += vector;
            curve.P3 += vector;
            return curve;
        }

        /// <summary>
        /// Translates an instance in a direction opposite to the given.
        /// </summary>
        /// <param name="curve"></param>
        /// <param name="vector"></param>
        public static BezierCubic operator -(BezierCubic curve, Vector3f vector)
        {
            curve.P0 -= vector;
            curve.P1 -= vector;
            curve.P2 -= vector;
            curve.P3 -= vector;
            return curve;
        }

        /// <summary>
        /// Multiplies an instance by a scalar.
        /// </summary>
        /// <param name="curve">The instance.</param>
        /// <param name="scale">The scalar.</param>
        /// <returns>The result of the calculation.</returns>
        public static BezierCubic operator *(BezierCubic curve, float scale)
        {
            curve.P0 *= scale;
            curve.P1 *= scale;
            curve.P2 *= scale;
            curve.P3 *= scale;
            return curve;
        }

        /// <summary>
        /// Multiplies an instance by a scalar.
        /// </summary>
        /// <param name="scale">The scalar.</param>
        /// /// <param name="curve">The instance.</param>
        /// <returns>The result of the calculation.</returns>
        public static BezierCubic operator *(float scale, BezierCubic curve)
        {
            curve.P0 *= scale;
            curve.P1 *= scale;
            curve.P2 *= scale;
            curve.P3 *= scale;
            return curve;
        }

        /// <summary>
        /// Divides an instance by a scalar.
        /// </summary>
        /// <param name="curve">The instance.</param>
        /// <param name="scale">The scalar.</param>
        /// <returns>The result of the calculation.</returns>
        public static BezierCubic operator /(BezierCubic curve, float scale)
        {
            float mult = 1.0f / scale;
            curve.P0 *= mult;
            curve.P1 *= mult;
            curve.P2 *= mult;
            curve.P3 *= mult;
            return curve;
        }

        /// <summary>
        /// Compares two instances for equality.
        /// </summary>
        /// <param name="left">The first instance.</param>
        /// <param name="right">The second instance.</param>
        /// <returns>True, if left equals right; false otherwise.</returns>
        public static bool operator ==(BezierCubic left, BezierCubic right)
        {
            return left.Equals(right);
        }

        /// <summary>
        /// Compares two instances for inequality.
        /// </summary>
        /// <param name="left">The first instance.</param>
        /// <param name="right">The second instance.</param>
        /// <returns>True, if left does not equa lright; false otherwise.</returns>
        public static bool operator !=(BezierCubic left, BezierCubic right)
        {
            return !left.Equals(right);
        }

        #endregion

        #region Overrides

        private static string listSeparator = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ListSeparator;
        
        /// <summary>
        /// Returns a System.String that represents the current BezierCubic.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return String.Format("{0}{4} {1}{4} {2}{4} {3}", P0.ToString(), P1.ToString(), P2.ToString(), P3.ToString(), listSeparator);
        }

        /// <summary>
        /// Returns the hashcode for this instance.
        /// </summary>
        /// <returns>A System.Int32 containing the unique hashcode for this instance.</returns>
        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = this.P0.GetHashCode();
                hashCode = (hashCode * 397) ^ this.P1.GetHashCode();
                hashCode = (hashCode * 397) ^ this.P2.GetHashCode();
                hashCode = (hashCode * 397) ^ this.P3.GetHashCode();
                return hashCode;
            }
        }

        /// <summary>
        /// Indicates whether this instance and a specified object are equal.
        /// </summary>
        /// <param name="obj">The object to compare to.</param>
        /// <returns>True if the instances are equal; false otherwise.</returns>
        public override bool Equals(object obj)
        {
            if (!(obj is BezierCubic))
                return false;

            return this.Equals((BezierCubic)obj);
        }

        #endregion

        #endregion

        #region IEquitable<BezierCubic> Members

        /// <summary>Indicates whether the current Bezier curve is equal to another Bezier curve.</summary>
        /// <param name="other">A curve to compare with this curve.</param>
        /// <returns>true if the current curve is equal to the curve parameter; otherwise, false.</returns>
        public bool Equals(BezierCubic other)
        {
            return
                P0 == other.P0 &&
                P1 == other.P1 &&
                P2 == other.P2 &&
                P3 == other.P3;
        }

        #endregion
    }
}
